import React from 'react'

// createContext is a method used to create a context in react
const ProductContext = React.createContext();

export const ProductProvider = ProductContext.Provider;

export default ProductContext;
// modules
import { useEffect, useState } from "react"
import styled from "styled-components"
import ActiveProductMod from "./ActiveProductMod";

// start of CSS
const Container = styled.div`
    padding: 20px;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
`
//end of CSS

//{cat, filters, sort}
const ActiveProducts = () => {
  const [activeProducts, setActiveProducts] = useState([]);

  useEffect(() => {

    fetch(`${process.env.REACT_APP_URI}/api/products/activeProducts`)
    .then(response => response.json())
    .then(data => {
        console.log(data);

        setActiveProducts(data.map(activeProduct => {
            return (<ActiveProductMod key={activeProduct._id} activeProduct={activeProduct}/>)
        }))
    })
   
  }, []);

  return (
    <Container>
        {activeProducts}
    </Container>
  )
}

export default ActiveProducts

// modules
import styled from 'styled-components';
import Navbar from '../components/Navbar';
import { Link } from 'react-router-dom';
import React, { useContext, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

// start of CSS
const Container = styled.div`
    width: 100%;
    height: 92vh;
    background-color: #e1e1e1;
    display: flex;
    align-items: center;
    justify-content: center;
`

const Wrapper = styled.div`
    width: 25%;
    min-width: 300px;
    padding: 20px;
    text-align: center;
`

const Title = styled.h2`
    font-size: 1.5em;
    font-weight: 300;
    text-transform: uppercase;
`

const Form = styled.form`
    margin-top: 20px;
    padding-right: 20px;
`

const Input = styled.input`
    text-align: center;
    width: 100%;
    margin: 0 0 20px 0;
    padding: 10px;

    box-shadow: inset 3px 3px 6px #aaa, inset -2px -2px 5px #ccc;
    transition: all 0.2s ease-in-out;
    appearance: none;
    -webkit-appearance: none;
    border: 0;
    outline: 0;
    font-size: 1rem;
    border-radius: 320px;
    background-color: #ddd;
`

const Button = styled.button`
    width: 100%;
    border: none;
    padding: 15px 20px;
    margin: 20px 0 20px 10px;
    cursor: pointer;
    background-color: #696969;
    border-radius: 320px;

    color: #fff;
    box-shadow: -2px -2px 5px #c0c0c0,
    5px 5px 10px #000;
    transition: all 0.2s ease-in-out;

    &:hover {
        box-shadow: -2px -2px 5px #c0c0c0,
        1.5px 1.5px 5px #000;
    }

    &:active {
        box-shadow: inset 1px 1px 2px #c0c0c0,
        inset -1px -1px 2px #000,
    }
`

const LoginPage = styled.p`
    font-size: 0.9em;
    margin-left: 20px;
`

const Login = styled.a`
    color: crimson;
    cursor: pointer;
`
// end of CSS

const CreateProduct = () => {
    const [title, setTitle] = useState("");
    const [price, setPrice] = useState('');
    const [description, setDescription] = useState("");
    const [images, setImages] = useState("");
    const [category, setCategory] = useState("");
    const [stock, setStock] = useState('');
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate();

    const {user} = useContext(UserContext);

    useEffect(() => {
        title !== "" && price !== "" && description !== "" && images !== "" && category !== "" && stock !== ""
        ? setIsActive(true)
        : setIsActive(false)

    }, [title, price, description, images, category, stock, isActive]);

    const createProduct = event => {
        event.preventDefault();

        console.log(user)
        if(user.isAdmin){
            fetch(`${process.env.REACT_APP_URI}/api/products/create`, {
                method: "POST",
                headers: {
                    'Content-Type' : 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    title,
                    price,
                    description,
                    images,
                    category,
                    stock
                })
                
            }).then(response => response.json())
            .then(data =>{
                console.log(data);
                    if(data.hasImage === false) {
                        Swal.fire({
                            title: "No Image Uploaded",
                            icon: "error",
                            text: "Pleas upload an image"
                        })
                    } else if (data.productExists === true) {
                        Swal.fire({
                            title: "Product Already Exists",
                            icon: "error",
                            text: "Please use another title for the product"
                        })
                    } else {
                        Swal.fire({
                            title: "Product Created",
                            icon: "success",
                            text: "Thank you for your patience"
                        })

                        navigate('/dashboard');
                    }
                })
        } else {
            Swal.fire({
                title: "Access Denied",
                icon: "success",
                text: "You are not authorized to do this type of action."
            })
        }
    }

    return (
        <>  
            <Container>
                <Wrapper>
                    <Title>Create Product</Title>
                    <Form onSubmit={createProduct}>
                        <Input
                            type="text"
                            placeholder="Title"
                            value={title}
                            onChange={event => setTitle(event.target.value)}
                            required/>
                        <Input
                            type="number"
                            placeholder="Price"
                            value={price}
                            onChange={event => setPrice(event.target.value)}
                            required/>
                        <Input
                            type="text"
                            placeholder="Description"
                            value={description}
                            onChange={event => setDescription(event.target.value)}
                            required/>
                        <Input
                            placeholder="Image"
                            type="text"
                            value={images}
                            onChange={event => setImages(event.target.value)}
                            required/>
                        <Input
                            placeholder="Category"
                            type="text"
                            value={category}
                            onChange={event => setCategory(event.target.value)}
                            required/>

                        <Input
                            placeholder="Stock"
                            type="number"
                            value={stock}
                            onChange={event => setStock(event.target.value)}
                            required/>

                        <Button type="submit">Create Product</Button>
                       
                    </Form>
                </Wrapper>
            </Container>
        </>
  )
}

export default CreateProduct;